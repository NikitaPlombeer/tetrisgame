package com.plombeer.tetrisgame;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;

import java.util.ArrayList;
import java.util.Random;
import java.util.Vector;

/**
 * Created by hh on 12.06.2015.
 */
public class Pole {

    public int N;
    public static final Color WHITE = new Color(255, 255, 255);
    private ArrayList<Color> standardColors;
    private Vector<ColorPoint> moved_blocks;

    enum TypeOfElement{
        m_meteor, um_meteor, empty
    }

    //Количество блоков, которое отображает экран по вертикали
    private int visibleH;

    //Матрица поля состоит из
    static class Element{
        Pole.TypeOfElement type;
        Color color;

        Element() {
            color = new Color();
            type = TypeOfElement.empty;
        }

        Element(Element element) {
            color = new Color();
            set(element);
        }

        Element(TypeOfElement t, int R, int G, int B){
            color = new Color(R, G, B);
            type = t;
        }

        Element(TypeOfElement t, Color color){
            this.color = color;
            type = t;
        }

        void set(Element element){
            type = element.type;
            color.R = element.color.R;
            color.G = element.color.G;
            color.B = element.color.B;
        }
    }

    private Element gamePole[][];
    private Vector<Type> types; // Типы фигур


    Pole(int N, int visibleH){
        this.N = N;
        this.visibleH = visibleH;
        moved_blocks = new Vector<ColorPoint>();

        loadTypes();

        standardColors = new ArrayList<Color>();
        standardColors.add(new Color(243, 66, 53));
        standardColors.add(new Color(232, 29, 98));
        standardColors.add(new Color(155, 38, 175));
        standardColors.add(new Color(102, 57, 182));
        standardColors.add(new Color(63, 81, 181));
        standardColors.add(new Color(33, 150, 243));
        standardColors.add(new Color(2, 168, 243));
        standardColors.add(new Color(0, 188, 212));
        standardColors.add(new Color(0, 150, 136));
        standardColors.add(new Color(254, 192, 6));
        standardColors.add(new Color(254, 234, 58));
        standardColors.add(new Color(204, 219, 56));
        standardColors.add(new Color(138, 194, 73));
        standardColors.add(new Color(75, 174, 79));
        standardColors.add(new Color(254, 151, 0));
        standardColors.add(new Color(254, 86, 33));
        standardColors.add(new Color(120, 84, 71));
        standardColors.add(new Color(157, 157, 157));
        standardColors.add(new Color(96, 125, 139));
        standardColors.add(WHITE);

        gamePole = new Element[N][N];
        for (int i = 0; i < N; i++) {
            for (int j = 0; j < N; j++) {
                gamePole[i][j] = new Element(TypeOfElement.empty, new Color(WHITE));
            }
        }

    }

    public int getVisibleH() {
        return visibleH;
    }

    public Element getElement(int x, int y){
        return gamePole[x][y];//.get(x).get(y);
    }

    public void setElement(Element t, int x, int y){
        gamePole[x][y].set(t);
    }

    public void setTypeOfElement(TypeOfElement type, int x, int y){
        gamePole[x][y].type = type;
    }

    public int getIndexByColor(Color color){
        for (int i = 0; i < standardColors.size(); i++) {
            if(standardColors.get(i).equals(color)) return i;
        }
        return -1;
    }


    public Color getColorByIndex(int index){
        if(index >= standardColors.size()) return new Color(0, 0, 0);
        return standardColors.get(index);
    }

    /**
     * Функция создает новую фигуру
     * @return True, если при повявлении новой фигуры, она не попадает на блок
     */
    public boolean makeNewFigure(){
        Random rand = new Random();
        int index = rand.nextInt(types.size());

        int index_c = rand.nextInt(standardColors.size() - 1);
        clearMovedBlocks();
        boolean check = true;
        for (int i = 0; i < types.get(index).W; i++) {
            for (int j = 0; j < types.get(index).H; j++) {
                if(types.get(index).figs[i][j]) {
                    if(getElement(N / 2 - types.get(index).W / 2 + i, N - j - 1).type.equals(TypeOfElement.um_meteor))
                        check = false;
                    setTypeOfElement(TypeOfElement.m_meteor, N / 2 - types.get(index).W / 2 + i, N - j - 1);
                    getElement(N / 2 - types.get(index).W / 2 + i, N - j - 1).color.set(getColorByIndex(index_c));
                    moved_blocks.add(new ColorPoint(N / 2 - types.get(index).W / 2 + i, N - j - 1, getColorByIndex(index_c)));
                }
            }
        }

        return check;
    }

    //Ищем и удаляем квадрат на поле
    public boolean findSquare(int planetSize, ArrayList<GameScreen.DeleteCube> deleteCubes){
        int planetX = N / 2 - planetSize / 2;
        int planetY = N / 2 - planetSize / 2;

        planetX--;
        planetY--;

        boolean qCheck = false;


        for (int size = planetSize + 2; size < visibleH; size += 2) {

            for (int j = planetX; j < planetX + size; j++) {
                if(!getElement(j, planetY).type.equals(TypeOfElement.um_meteor) ||
                        !getElement(planetX, j).type.equals(TypeOfElement.um_meteor) ||
                        !getElement(j, planetY + size - 1).type.equals(TypeOfElement.um_meteor) ||
                        !getElement(planetX + size - 1, j).type.equals(TypeOfElement.um_meteor)) {
                    qCheck = true;
                    break;
                }
            }

            if(!qCheck){
                for (int j = planetX; j < planetX + size; j++) {
                    deleteCubes.add(new GameScreen.DeleteCube(j, planetY, getElement(j, planetY).color, GameScreen.METEOR_SIZE));
                    deleteCubes.add(new GameScreen.DeleteCube(planetX, j, getElement(planetX, j).color, GameScreen.METEOR_SIZE));
                    deleteCubes.add(new GameScreen.DeleteCube(j, planetY + size - 1, getElement(j, planetY + size - 1).color, GameScreen.METEOR_SIZE));
                    deleteCubes.add(new GameScreen.DeleteCube(planetX + size - 1, j, getElement(planetX + size - 1, j).color, GameScreen.METEOR_SIZE));

                    setTypeOfElement(TypeOfElement.empty, j, planetY);
                    setTypeOfElement(TypeOfElement.empty, planetX, j);
                    setTypeOfElement(TypeOfElement.empty, j, planetY + size - 1);
                    setTypeOfElement(TypeOfElement.empty, planetX + size - 1, j);
                }
            }
            planetX--;
            planetY--;
            qCheck = false;
        }
        return !qCheck;
    }

    //Загружаем фигуры из файла в Vector<Type> types
    private void loadTypes(){
        types = new Vector<Type>();
        FileHandle file = Gdx.files.internal("figures.txt");
        String s = file.readString();
        String str[] = s.split("\r\n");
        for (int i = 0; i < str.length; i++) {
            String size[] = str[i].split(" ");
            int W = Integer.parseInt(size[0]);
            int H = Integer.parseInt(size[1]);
            boolean fig[][] = new boolean[W][H];

            for (int j = 0; j < H; j++) {
                String stroke[] = str[i + j + 1].split(" ");
                for (int k = 0; k < W; k++) {
                    fig[k][j] = stroke[k].equals("1") ? true : false;
                }
            }
            types.add(new Type(W, H, fig));
            i += H;
        }
    }

    enum TypeOfMove{
        down, right, left
    }

    public void addMoveBlock(int x, int y, Color color){
        moved_blocks.add(new ColorPoint(x, y, color));
    }

    public void clearMovedBlocks(){
        moved_blocks.clear();
    }
    public void addMoveBlock(int x, int y){
        moved_blocks.add(new ColorPoint(x, y));
    }

    //Двигаем фигуру внизи или впрво или влево, в зависимости от t
    public boolean move(TypeOfMove t){
        Point m = new Point(0, 0);
        switch (t){
            case down:
                m.set(0, -1);
                break;
            case left:
                m.set(-1, 0);
                break;
            case right:
                m.set(1, 0);
                break;
        }

        boolean qCheck = true;

        for (int i = 0; i < this.moved_blocks.size(); i++) {
            int x = this.moved_blocks.get(i).x;
            int y = this.moved_blocks.get(i).y;
            
            if(x + m.x < N && y + m.y < N && x + m.x >= 0 && y + m.y >= 0 && (getElement(x + m.x, y + m.y).type.equals(TypeOfElement.um_meteor) || y + m.y == 0)){
                if(y + m.y == 0) GameScreen.lifes--;
                qCheck = false;
                break;
            }
        }

        if(qCheck) {
            for (int i = 0; i < moved_blocks.size(); i++) {
                setTypeOfElement(TypeOfElement.empty, moved_blocks.get(i).x, moved_blocks.get(i).y);
                getElement(moved_blocks.get(i).x, moved_blocks.get(i).y).color.set(255, 255, 255);

            }
            for (int i = 0; i < moved_blocks.size(); i++) {
                setTypeOfElement(TypeOfElement.m_meteor, (moved_blocks.get(i).x + m.x), (moved_blocks.get(i).y + m.y));
                getElement(moved_blocks.get(i).x + m.x, moved_blocks.get(i).y + m.y).color.set(moved_blocks.get(i).color);
                moved_blocks.get(i).set(moved_blocks.get(i).x + m.x, moved_blocks.get(i).y + m.y);
            }
        }else{

            if(t.equals(TypeOfMove.down)) {
                for (int i = 0; i < moved_blocks.size(); i++) {
                    setTypeOfElement(TypeOfElement.um_meteor, moved_blocks.get(i).x, moved_blocks.get(i).y);

                }
                moved_blocks.clear();
            }
            return false;
        }
        return true;
    }
}
