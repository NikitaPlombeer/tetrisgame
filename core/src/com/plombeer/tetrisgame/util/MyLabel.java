package com.plombeer.tetrisgame.util;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.math.Vector2;

/**
 * Created by hh on 08.07.2015.
 */
public class MyLabel {

    private BitmapFont font;
    private Vector2 position;
    private String text;
    public boolean visible = true;
    private float size = 124;


    // Russian cyrillic characters
    public static final String CHARACTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
            + "abcdefghijklmnopqrstuvwxyz"
            + "1234567890:";

    public String GAME_FONT_NAME;

    public MyLabel(String GAME_FONT_NAME) {
        this.GAME_FONT_NAME = GAME_FONT_NAME;
        font = generateFont(GAME_FONT_NAME, CHARACTERS);
        position = new Vector2();
        text = "";
    }

    public float getSize() {
        return size;
    }

    public MyLabel(String GAME_FONT_NAME, String text) {
        this.GAME_FONT_NAME = GAME_FONT_NAME;
        font = generateFont(GAME_FONT_NAME, CHARACTERS);
        position = new Vector2();
        this.text = text;
    }

    public MyLabel(String GAME_FONT_NAME, float x, float y) {
        this.GAME_FONT_NAME = GAME_FONT_NAME;
        font = generateFont(GAME_FONT_NAME, CHARACTERS);
        position = new Vector2(x, y);
        text = "";
    }

    public MyLabel(String GAME_FONT_NAME, float x, float y, String text) {
        this.GAME_FONT_NAME = GAME_FONT_NAME;
        font = generateFont(GAME_FONT_NAME, CHARACTERS);
        position = new Vector2(x, y);
        this.text = text;
    }

    public void setVisibility(boolean visibility){
        visible = visibility;
    }

    public void setText(String text) {
        this.text = text;
    }

    public void setColor(Color color){
        font.setColor(color);
    }

    public void setSize(float newSize){
        font.setScale(newSize / size);
        size = newSize;
    }

    public void setPosition(float x, float y) {
        position.set(x, y);
    }

    public void setY(float y){
        position.y = y;
    }

    public void setX(float x){
        position.x = x;
    }

    public float getY(){
        return position.y;
    }

    public float getX(){
        return position.x;
    }

    public void translateY(float yAmount){
        position.y += yAmount;
    }

    public void translateX(float xAmount){
        position.x += xAmount;
    }


    public void setWidth(float width){
        setSize(size * (width / getWidth()));
    }

    public void draw(SpriteBatch batch){
        if(visible)
            font.draw(batch, text, position.x, position.y + getHeight());
    }

    public float getWidth(){
        return font.getBounds(text).width;
    }

    public float getHeight(){
        return font.getBounds(text).height;
    }

    private BitmapFont generateFont(String fontName, String characters) {

        // Configure font parameters
        FreeTypeFontGenerator.FreeTypeFontParameter parameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
        parameter.characters = characters;
        parameter.size = (int) size;
        Color color = new Color(1f, 1f, 1f, 1f);
        parameter.color = color;
        // Generate font
        FreeTypeFontGenerator generator = new FreeTypeFontGenerator( Gdx.files.internal(fontName) );
        BitmapFont newFont = generator.generateFont(parameter);

        // Dispose resources
        generator.dispose();

        return newFont;
    }
}
