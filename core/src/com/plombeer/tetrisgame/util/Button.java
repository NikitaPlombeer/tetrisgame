package com.plombeer.tetrisgame.util;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.plombeer.tetrisgame.ClikedInterface;

/**
 * Created by hh on 08.07.2015.
 */
public class Button extends Rectangle{

    Texture background;
    MyLabel label;
    private ClikedInterface clikedInterface;
    public boolean visible = true;
    public boolean enabled = true;

    public void setClikedInterface(ClikedInterface clikedInterface) {
        this.clikedInterface = clikedInterface;
    }

    public Button(float x, float y, float width, float height, String filename, String GAME_FONT_NAME, String text, Color textColor) {
        super(x, y, width, height);
        label = new MyLabel(GAME_FONT_NAME, text);
        label.setWidth(width / 1.5f);
        label.setPosition(x + width / 2f - label.getWidth() / 2, y + height / 2f - label.getHeight() / 2f);
        label.setColor(textColor);
        background = new Texture(filename);
    }

    public Button(float x, float y, float width, float height, String filename, String GAME_FONT_NAME, String text, Color textColor, float textSize) {
        super(x, y, width, height);
        label = new MyLabel(GAME_FONT_NAME, text);
        label.setSize(textSize);
        label.setPosition(x + width / 2f - label.getWidth() / 2, y + height / 2f - label.getHeight() / 2f);
        label.setColor(textColor);
        background = new Texture(filename);
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void draw(SpriteBatch batch){
        if(visible) {
            batch.draw(background, x, y, width, height);
            label.draw(batch);
        }
    }

    public float getTextSize(){
        return label.getSize();
    }
    public void setTextSize(float newSize){
        label.setSize(newSize);
        set_position(x, y);
    }
    public void set_y(float y){
        this.y = y;
        label.setY(y + height / 2f - label.getHeight() / 2f);
    }

    public void set_position(float x, float y){
        setPosition(x, y);
        label.setPosition(x + width / 2f - label.getWidth() / 2, y + height / 2f - label.getHeight() / 2f);
    }

    public void translateY(float yAmount){
        y += yAmount;
        label.translateY(yAmount);
    }

    public boolean onClick(int x, int y){
        if(x > this.x && y > this.y && x < this.x + this.width && y < this.y + this.height){
            if(clikedInterface != null) {
                clikedInterface.onClick();
                return true;
            }
        }
        return false;
    }

}
