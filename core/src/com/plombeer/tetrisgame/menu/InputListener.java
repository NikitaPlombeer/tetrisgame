package com.plombeer.tetrisgame.menu;

import com.badlogic.gdx.InputProcessor;
import com.plombeer.tetrisgame.GameScreen;
import com.plombeer.tetrisgame.ImageButton;

/**
 * Created by hh on 11.07.2015.
 */
public class InputListener implements InputProcessor{
    ImageButton iButton;
    MenuScreen menuScreen;

    public InputListener(MenuScreen menuScreen) {
        this.menuScreen = menuScreen;
    }

    @Override
    public boolean keyDown(int keycode) {
        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        if(menuScreen.newGameButton.onTouch(screenX, GameScreen.SCREEN_HEIGHT - screenY)) iButton = menuScreen.newGameButton;
        if(menuScreen.resumeButton.onTouch(screenX, GameScreen.SCREEN_HEIGHT - screenY)) iButton = menuScreen.resumeButton;
        if(menuScreen.settingsButton.onTouch(screenX, GameScreen.SCREEN_HEIGHT - screenY)) iButton = menuScreen.settingsButton;
        return true;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        if(iButton != null){
            if(iButton.equals(menuScreen.resumeButton)) menuScreen.resumeButton.onClick(screenX, GameScreen.SCREEN_HEIGHT - screenY);
            if(iButton.equals(menuScreen.newGameButton)) menuScreen.newGameButton.onClick(screenX, GameScreen.SCREEN_HEIGHT - screenY);
            if(iButton.equals(menuScreen.settingsButton)) menuScreen.settingsButton.onClick(screenX, GameScreen.SCREEN_HEIGHT - screenY);
        }
        iButton = null;
        return true;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }
}
