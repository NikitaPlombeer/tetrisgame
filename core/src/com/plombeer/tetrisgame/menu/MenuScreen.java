package com.plombeer.tetrisgame.menu;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.plombeer.tetrisgame.ClikedInterface;
import com.plombeer.tetrisgame.GameScreen;
import com.plombeer.tetrisgame.ImageButton;
import com.plombeer.tetrisgame.MiniTimer;
import com.plombeer.tetrisgame.MyGdxGame;
import com.plombeer.tetrisgame.Tasker;
import com.plombeer.tetrisgame.settings.SettingScreen;
import com.plombeer.tetrisgame.util.MyLabel;

/**
 * Created by hh on 11.07.2015.
 */
public class MenuScreen implements Screen{

    private final float buttonSize = GameScreen.SCREEN_WIDTH / 4f;
    public ImageButton newGameButton;
    private MyLabel newGameLabel;
    public ImageButton settingsButton;
    private MyLabel settingsLabel;
    public ImageButton resumeButton;
    private MyLabel resumeLabel;
    private SpriteBatch batch;

    private MiniTimer resumeButtonTimer;
    private MiniTimer newGameButtonTimer;
    private MiniTimer settingsButtonTimer;

    MyGdxGame game;

    public MenuScreen(MyGdxGame game) {
        this.game = game;
    }

    public final float START_X = GameScreen.SCREEN_WIDTH / 5f;
    public final float START_Y = GameScreen.SCREEN_HEIGHT / 2f + (2.3f * buttonSize) / 2f;

    boolean canTick = false;
    public static boolean canResume = false;

    public boolean canResume(){
        FileHandle file = Gdx.files.external("NewTetris/lastgame.txt");
        return file.exists();
    }
    @Override
    public void show() {
        canTick = false;
        FileHandle file = Gdx.files.external("NewTetris/lastgame.txt");
        canResume = file.exists();

        Color ntText = new Color(SettingScreen.theme.equals(SettingScreen.ColorTheme.light) ? GameScreen.LIGHT_THEME_TEXT_COLOR : GameScreen.DARK_THEME_TEXT_COLOR);
        ntText.a = 1f;

        batch = new SpriteBatch();
        resumeButton = new ImageButton( -GameScreen.SCREEN_WIDTH,  START_Y, buttonSize, buttonSize, "textures/menu/resume_button.png", false, false, canResume ? 1f : 0.5f);
        newGameButton = new ImageButton(-GameScreen.SCREEN_WIDTH, resumeButton.y - buttonSize * 1.3f, buttonSize, buttonSize, "textures/menu/newGame_button.png", false, false, 1f);
        settingsButton = new ImageButton(-GameScreen.SCREEN_WIDTH, newGameButton.y - buttonSize * 1.3f, buttonSize, buttonSize, "textures/menu/settings_button.png", false, false, 1f);

        newGameLabel = new MyLabel(GameScreen.MAIN_TTF, newGameButton.x + newGameButton.width * 1.3f, newGameButton.y + newGameButton.height / 2f, "new game");
        newGameLabel.setColor(ntText);
        newGameLabel.setSize(buttonSize / 4f);
        newGameLabel.translateY(-newGameLabel.getHeight() / 2);

        resumeLabel = new MyLabel(GameScreen.MAIN_TTF, resumeButton.x + resumeButton.width * 1.3f, resumeButton.y + resumeButton.height / 2f, "resume game");
        if(!canResume) ntText.a = 0.5f;
        resumeLabel.setColor(ntText);
        ntText.a = 1f;
        resumeLabel.setSize(buttonSize / 4f);
        resumeLabel.translateY(-resumeLabel.getHeight() / 2);

        settingsLabel = new MyLabel(GameScreen.MAIN_TTF, settingsButton.x + settingsButton.width * 1.3f, settingsButton.y + settingsButton.height / 2f, "settings");
        settingsLabel.setColor(ntText);
        settingsLabel.setSize(buttonSize / 4f);
        settingsLabel.translateY(-settingsLabel.getHeight() / 2);


        resumeButton.setClikedInterface(new ClikedInterface() {
            @Override
            public void onClick() {
                if(canResume){
                    GameScreen.typeOfOpen = GameScreen.TypeOfOpen.resume;
                    game.setScreen(game.gameScreen);
                }
            }
        });
        newGameButton.setClikedInterface(new ClikedInterface() {
            @Override
            public void onClick() {
                GameScreen.typeOfOpen = GameScreen.TypeOfOpen.newGame;
                canResume = true;
                game.setScreen(game.gameScreen);
            }
        });

        settingsButton.setClikedInterface(new ClikedInterface() {
            @Override
            public void onClick() {
                game.setScreen(game.settingsScreen);
            }
        });

        resumeButtonTimer = new MiniTimer(true, 0.5f);
        final float vel_r = (START_X + GameScreen.SCREEN_WIDTH) / resumeButtonTimer.getInterval();
        resumeButtonTimer.setTasker(new Tasker() {
            @Override
            public void doAfterTime() {
                resumeButton.setPos(START_X, START_Y);
                resumeLabel.setX(resumeButton.x + resumeButton.width * 1.3f);
            }

            @Override
            public void onTick(float delta) {
                resumeButton.translateX(delta * vel_r);
                resumeLabel.translateX(delta * vel_r);
            }
        });

        newGameButtonTimer = new MiniTimer(true, 0.7f);
        final float vel_n = (START_X + GameScreen.SCREEN_WIDTH) / newGameButtonTimer.getInterval();
        newGameButtonTimer.setTasker(new Tasker() {
            @Override
            public void doAfterTime() {
                newGameButton.setPos(START_X, newGameButton.y);
                newGameLabel.setX(newGameButton.x + newGameButton.width * 1.3f);
            }

            @Override
            public void onTick(float delta) {
                newGameButton.translateX(delta * vel_n);
                newGameLabel.translateX(delta * vel_n);
            }
        });

        settingsButtonTimer = new MiniTimer(true, 0.9f);
        final float vel_s = (START_X + GameScreen.SCREEN_WIDTH) / settingsButtonTimer.getInterval();
        settingsButtonTimer.setTasker(new Tasker() {
            @Override
            public void doAfterTime() {
                settingsButton.setPos(START_X, settingsButton.y);
                settingsLabel.setX(settingsButton.x + settingsButton.width * 1.3f);
            }

            @Override
            public void onTick(float delta) {
                settingsButton.translateX(delta * vel_s);
                settingsLabel.translateX(delta * vel_s);
            }
        });

        Gdx.input.setInputProcessor(new InputListener(this));

    }

    @Override
    public void render(float delta) {
        if(SettingScreen.theme.equals(SettingScreen.ColorTheme.light))
            Gdx.gl.glClearColor(GameScreen.LIGHT_THEME_BACKGROUND_COLOR.r, GameScreen.LIGHT_THEME_BACKGROUND_COLOR.g, GameScreen.LIGHT_THEME_BACKGROUND_COLOR.b, 1f); else
            Gdx.gl.glClearColor(GameScreen.DARK_THEME_BACKGROUND_COLOR.r, GameScreen.DARK_THEME_BACKGROUND_COLOR.g, GameScreen.DARK_THEME_BACKGROUND_COLOR.b, 1f);

        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        resumeButton.draw(batch);
        newGameButton.draw(batch);
        settingsButton.draw(batch);

        batch.begin();
        newGameLabel.draw(batch);
        resumeLabel.draw(batch);
        settingsLabel.draw(batch);
        batch.end();

        if(canTick) {
            resumeButtonTimer.tick(delta);
            newGameButtonTimer.tick(delta);
            settingsButtonTimer.tick(delta);
        } else canTick = true;
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        batch.dispose();
    }
}
