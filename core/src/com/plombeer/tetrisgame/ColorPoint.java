package com.plombeer.tetrisgame;

/**
 * Created by hh on 14.07.2015.
 */
public class ColorPoint extends Point{
    Color color;
    public ColorPoint(int x, int y) {
        super(x, y);
        color = new Color(Pole.WHITE);
    }

    public ColorPoint(int x, int y, Color color) {
        super(x, y);
        this.color = new Color();
        this.color.set(color);
    }


    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }
}
