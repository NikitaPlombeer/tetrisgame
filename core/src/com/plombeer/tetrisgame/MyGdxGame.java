package com.plombeer.tetrisgame;

import com.badlogic.gdx.Game;
import com.plombeer.tetrisgame.menu.MenuScreen;
import com.plombeer.tetrisgame.settings.SettingScreen;
import com.plombeer.tetrisgame.util.ShowAds;

public class MyGdxGame extends Game {

    public GameScreen gameScreen;
    public MenuScreen menuScreen;
    public SettingScreen settingsScreen;

    ShowAds showAds;
    @Override
    public void create() {
        gameScreen = new GameScreen(this);
        menuScreen = new MenuScreen(this);
        settingsScreen = new SettingScreen(this);
        SettingScreen.getSettings();
        setScreen(menuScreen);
    }


    public MyGdxGame(ShowAds showAds) {
        this.showAds = showAds;
    }


    public MyGdxGame() {
    }

    @Override
    public void dispose() {
    }
}
