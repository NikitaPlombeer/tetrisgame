package com.plombeer.tetrisgame;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;

/**
 * Created by hh on 29.06.2015.
 */
public class ImageButton extends Rectangle{

    private Sprite texture;
    private float alpha;
    private ClikedInterface clikedInterface;
    boolean flip;
    public boolean visible = true;
    public boolean enabled = true;
    public ImageButton(float x, float y, float width, float height, String filename, boolean flip, boolean rotate90, float alpha) {
        super(x, y, width, height);
        texture = new Sprite(new Texture(filename));
        this.flip = flip;
        texture.setAlpha(alpha);
        this.alpha = alpha;
        texture.setPosition(x, y);
        texture.setSize(width, height);
        if(rotate90)
            texture.rotate90(false);
        texture.flip(flip, false);
        texture.setOriginCenter();
    }

    public Sprite getTexture() {
        return texture;
    }

    public void draw(SpriteBatch batch){
        if(visible) {
            batch.begin();
            texture.draw(batch);
            batch.end();
        }
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void disable() {
        enabled = false;
    }

    public void enable() {
        enabled = true;
    }

    public void setClikedInterface(ClikedInterface clikedInterface) {
        this.clikedInterface = clikedInterface;
    }

    public boolean onTouch(int x, int y) {
        if(isEnabled()) {
            if (x > this.x && y > this.y && x < this.x + this.width && y < this.y + this.height) {
                texture.setAlpha(1f);
                return true;
            }
        }
        return false;
    }

    public void translateWidth(float wAmount){
        width += wAmount;
        texture.setSize(texture.getWidth() + wAmount, texture.getHeight());
    }

    public void translateHeight(float hAmount){
        height += hAmount;
        texture.setSize(texture.getWidth(), texture.getHeight() + hAmount);
    }

    public void translateSize(float wAmount, float hAmount){
        width += wAmount;
        height += hAmount;
        texture.setSize(texture.getWidth() + wAmount, texture.getHeight() + hAmount);
    }

    public void set_width(float width){
        this.width = width;
        texture.setSize(width, texture.getHeight());
    }

    public void set_height(float height){
        this.height = height;
        texture.setSize(texture.getWidth(), height);
    }

    public boolean onClick(int x, int y){
        if(isEnabled()) {
            texture.setAlpha(alpha);
            if (x > this.x && y > this.y && x < this.x + this.width && y < this.y + this.height) {
                if (clikedInterface != null) {
                    clikedInterface.onClick();
                    return true;
                }
            }
        }
        return false;
    }

    public void translateY(float deltaY){
        y += deltaY;
        texture.translateY(deltaY);
    }

    public void changeTexture(String filename){
        texture =new Sprite(new Texture(filename));
        setPos(x, y);
        texture.setSize(width, height);
        texture.setAlpha(alpha);
        texture.flip(flip, false);
    }

    public void translateX(float deltaX){
        x += deltaX;
        texture.translateX(deltaX);
    }

    public void setPos(float x, float y){
        texture.setPosition(x, y);
        this.x = x;
        this.y = y;
    }
}

