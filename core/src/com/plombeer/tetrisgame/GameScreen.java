package com.plombeer.tetrisgame;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.plombeer.tetrisgame.menu.MenuScreen;
import com.plombeer.tetrisgame.settings.SettingScreen;
import com.plombeer.tetrisgame.util.Button;
import com.plombeer.tetrisgame.util.MyLabel;

import java.util.ArrayList;

/**
 * Created by hh on 12.06.2015.
 */
public class GameScreen implements Screen{

    public enum TypeOfOpen{
        resume, newGame
    }

    public static TypeOfOpen typeOfOpen = TypeOfOpen.newGame;

    MyGdxGame game;
    public GameScreen(MyGdxGame game) {
        this.game = game;
    }

    public static final String ROOT_DIRECTORY = "NewTetris/";
    public static int SCREEN_WIDTH = Gdx.graphics.getWidth();
    public static int SCREEN_HEIGHT = Gdx.graphics.getHeight();
    public static float METEOR_SIZE; //Размер каждого блока
    public static com.badlogic.gdx.graphics.Color LIGHT_THEME_TEXT_COLOR = new com.badlogic.gdx.graphics.Color(51 / 255f, 51 / 255f, 51 / 255f, 0.5f);
    public static com.badlogic.gdx.graphics.Color DARK_THEME_TEXT_COLOR = new com.badlogic.gdx.graphics.Color(204 / 255f, 204 / 255f, 204 / 255f, 0.5f);
    public static final com.badlogic.gdx.graphics.Color LIGHT_THEME_BACKGROUND_COLOR = new com.badlogic.gdx.graphics.Color(248 / 255f, 248 / 255f, 1f, 1f);
    public static final com.badlogic.gdx.graphics.Color DARK_THEME_BACKGROUND_COLOR = new com.badlogic.gdx.graphics.Color(36 / 255f, 36 / 255f, 36 / 255f, 1f);
    public static com.badlogic.gdx.graphics.Color LIGHT_THEME_2ND_TEXT_COLOR = new com.badlogic.gdx.graphics.Color(1 / 255f, 14 / 255f, 118 / 255f, 1f);
    public static com.badlogic.gdx.graphics.Color DARK_THEME_2ND_TEXT_COLOR = new com.badlogic.gdx.graphics.Color(254 / 255f, 241 / 255f, 137 / 255f, 1f);
    private float EPSILON; //Отступ, чтобы кубы не прилипали друг к другу
    public static final String MAIN_TTF = "fonts/junegull.ttf";

    private SpriteBatch m_batch;
    private ShapeRenderer m_shapeRenderer; //Данный ShapeRenderer отвечает за отрисовку, движущихся фигур
    private ShapeRenderer um_shapeRenderer;//Данный ShapeRenderer отвечает за отрисовку, не движущихся фигур

    /**
     * Поле игры.
     * Включает в себя матрицу, в которой записан тип каждой клеточки и ее цвет - > ArrayList<ArrayList<Element>> gamePole
     * Включает список стандартных цветов блоков -> ArrayList<Color> standardColors
     * Включает количество блоков, которое отображает экран по вертикали -> visibleH
     * (по горизонтали отображает количество блоков, которые убирается на экране)
     * Включет списов типов фигур, которые берутся из файла "figures.txt" ->  Vector<Type> types
     */
    private Pole pole;

    //Таймеры для анимации
    private MiniTimer moveTimer; //Таймер падения фигуры
    private MiniTimer rotateTimer; //Таймер поворта фигуры
    private MiniTimer deleteTimer; //Таймер исчезания блоков, которые выстроились в квадрат
    private MiniTimer gameOverTimer; //Таймер выпадания надписи GameOver и кнопки NewGame
    private MiniTimer scoreTimer; //Таймер выпадания надписи GameOver и кнопки NewGame
    private MiniTimer putDownTimer; //Таймер выпадания надписи GameOver и кнопки NewGame
    private MiniTimer pauseRotateTimer;

    private OrthographicCamera camera;

    private float angle = 0f; //Угол на который планета уже повернулась во время анимации
    private float angleVel; //Скорость поворота планеты

    /**
     * Так поле квадратное, у нас отображается не вся матрица на экране, поэтому
     * нужна координата (x, y), которая будет началом координат в нашей игры
     * -> Vector2 start;
     */
    private Vector2 startPos;

    private int planetSize;
    private Point planetPos;
    private Rectangle planet;

    //Кнопки управления
    public ImageButton leftButton;
    public ImageButton rightButton;
    public ImageButton rotateButton;
    public ImageButton downButton;
    public ImageButton pauseButton;
    public ImageButton restartButton;
    public Button exitButton;

    private MyLabel pauseLabel;
    private MyLabel gameOverLabel;
    private MyLabel scoreLabel;
    private MyLabel bestScoreLabel;

    public boolean qLose = false;
    private int score = 0;
    private int best_score = 0;
    private int maxTimerScore = 0;
    private Sprite heart;
    public static int lifes;

    /**
     * Массив кубов, которые удаляются. Сделан для анимации удаления,
     * в которой мы постепенно уменьшаем deleteCubes.get(i).size
     */
    private ArrayList<DeleteCube> deleteCubes;

    public Pole getPole() {
        return pole;
    }

    /**
     * Рисует прямоугольник с закругленными углами
     * @param shapeRenderer место для рисования
     * @param x координата x
     * @param y координата y
     * @param width ширина прямоугольника
     * @param height высота прямоугольника
     * @param r радиус закругения углов
     * @param color цвет прямоугольника
     */
    private void drawRoundRect(ShapeRenderer shapeRenderer, float x, float y, float width, float height, float r, Color color){
        shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
        shapeRenderer.setColor(color.R / 255f, color.G / 255f, color.B / 255f, 1f);
        shapeRenderer.rect(x + r, y, width - 2 * r, height);
        shapeRenderer.rect(x, y + r, width, height - 2 * r);

        shapeRenderer.arc(x + width - r, y + height - r, r, 0, 90);
        shapeRenderer.arc(x + r, y + height - r, r, 90, 90);
        shapeRenderer.arc(x + r, y + r, r, 180, 90);
        shapeRenderer.arc(x + width - r, y + r, r, 270, 90);

        shapeRenderer.end();
    }

    public Point getPointByCoordinate(float x, float y){
        return new Point((int)((x + startPos.x) / METEOR_SIZE) , (int)((y + startPos.y) / METEOR_SIZE));
    }

    /**
     * Рисует прямоугольник с закругленными углами
     * @param shapeRenderer место для рисования
     * @param x координата x
     * @param y координата y
     * @param width ширина прямоугольника
     * @param height высота прямоугольника
     * @param r радиус закругения углов
     * @param color цвет прямоугольника
     */
    private void drawLinedRoundRect(ShapeRenderer shapeRenderer, float x, float y, float width, float height, float r, Color color){
        shapeRenderer.begin(ShapeRenderer.ShapeType.Line);
        shapeRenderer.setColor(color.R / 255f, color.G / 255f, color.B / 255f, 1f);

        shapeRenderer.line(x + r, y, x + width - r, y);
        shapeRenderer.line(x + r, y + height, x + width - r, y + height);
        shapeRenderer.line(x, y + r, x, y + height - r);
        shapeRenderer.line(x + width, y + r, x + width, y + height - r);

        
        //Math.max(1, (int)(6 * (float)Math.cbrt(r) * (90 / 360.0f)))
        drawArc(shapeRenderer, x + width - r, y + height - r, r, (int)r, 90, 90);
        drawArc(shapeRenderer, x + r, y + height - r, r, (int)r, 180, 90);
        drawArc(shapeRenderer, x + r, y + r, r, (int)r, 270, 90);
        drawArc(shapeRenderer, x + width - r, y + r, r, (int)r, 0, 90);

//        shapeRenderer.arc(x + r, y + height - r, r, 90, 90);
//        shapeRenderer.arc(x + r, y + r, r, 180, 90);
//        shapeRenderer.arc(x + width - r, y + r, r, 270, 90);
//
//
//        com.badlogic.gdx.graphics.Color color_back = SettingScreen.theme.equals(SettingScreen.ColorTheme.light) ? LIGHT_THEME_BACKGROUND_COLOR : DARK_THEME_BACKGROUND_COLOR;
//        shapeRenderer.setColor(color_back);
//        Point p = getPointByCoordinate(x + width - r + 1, y + height - r + 1);
//
//        if(!pole.getElement(p.x, p.y).type.equals(Pole.TypeOfElement.empty))
//            shapeRenderer.setColor(pole.getElement(p.x, p.y).color.toColor());
//        shapeRenderer.line(x + width - r, y + height - r, x + width, y + height - r);
//        shapeRenderer.line(x + width - r, y + height - r, x + width - r, y + height);
//        shapeRenderer.setColor(color_back);
//
//        shapeRenderer.line(x + r, y + height - r, x + r, y + height);
//        shapeRenderer.line(x + r, y + height - r, x, y + height - r);
//
//        shapeRenderer.line(x + r, y + r, x, y + r);
//        shapeRenderer.line(x + r, y + r, x + r, y);
//
//        shapeRenderer.line(x + width - r, y + r, x + width, y + r);
//        shapeRenderer.line(x + width - r, y + r, x + width - r, y);

        shapeRenderer.end();
    }

    static class DeleteCube{
        int x;
        int y;
        Color color;
        float size;

        DeleteCube(int x, int y, Color color, float size) {
            this.x = x;
            this.y = y;
            this.color = color;
            this.size = size;
        }
    }

    public static com.badlogic.gdx.graphics.Color lightToDark(com.badlogic.gdx.graphics.Color color){
        return new com.badlogic.gdx.graphics.Color(255f - 255f*color.r, 255f - 255f*color.g, 255f - 255f*color.g, color.a);
    }

    public void deleteLastGameFile(){
        FileHandle file = Gdx.files.external(ROOT_DIRECTORY+"lastgame.txt");
        if(file.exists()) file.delete();
    }
    /**
     * Функция проигрыша выключает все таймеры и включает gameOverTimer
     */

    int loseTimes = 0;
    public void loseFunction(){
        loseTimes++;
        if(loseTimes == 2){
            if(game.showAds != null)
                game.showAds.show();
            loseTimes = 0;
        }

        deleteLastGameFile();
        if(best_score < score)
            best_score = score;
        saveBestScore();

        restartButton.disable();
        leftButton.disable();
        rightButton.disable();
        downButton.disable();
        rotateButton.disable();


        moveTimer.disabled();
        deleteTimer.disabled();
        rotateTimer.disabled();
        gameOverLabel.setVisibility(true);
        gameOverTimer.enabled();
    }


    /**
     * Начало новой игры ->
     * 1. Отчищение поля игры
     * 2. Ставим надпись GameOver и кнопку NewGame на свои начальные места
     * 3. Создаем сразу новую фигуру
     * 4. Включаем таймер движения
     */
    public void newGame(){
        score = 0;
        qLose = false;
        lifes = 3;

        gameOverLabel.setVisibility(false);
        for (int i = 0; i < pole.N; i++) {
            for (int j = 0; j < pole.N; j++) {
                if(!(i >= planetPos.x && i < planetPos.x + planetSize && j >= planetPos.y && j < planetPos.y + planetSize)){
                    pole.setTypeOfElement(Pole.TypeOfElement.empty, i, j);
                    pole.getElement(i, j).color.set(255, 255, 255);
                }
            }
        }

        exitButton.set_position(SCREEN_WIDTH / 2f - SCREEN_WIDTH / 10f, SCREEN_HEIGHT + 3f);
        newGameButton1.set_position(SCREEN_WIDTH / 2f - SCREEN_WIDTH / 8f, exitButton.y + exitButton.getHeight()  + newGameButton1.getHeight() * 0.5f);
        gameOverLabel.setPosition(SCREEN_WIDTH / 2f - gameOverLabel.getWidth() / 2f, newGameButton1.y + newGameButton1.getHeight() + 1.5f * gameOverLabel.getHeight());

        moveTimer.enabled();
    }

    boolean needDrawHelpLines = false;
    public Point pointForHelpLines;

    public void countUpPointForHelpLines(int x, int y){

        if(y <= SCREEN_HEIGHT / 2 + SCREEN_WIDTH && y >= SCREEN_HEIGHT / 2 - SCREEN_WIDTH) {
            Point p = getPointByCoordinate(x, y);
            pointForHelpLines.set(p.x, p.y);
        }
    }

    public Point getPlanetPos() {
        return planetPos;
    }

    public int getPlanetSize() {
        return planetSize;
    }

    public void drawArc(ShapeRenderer shapeRenderer, float x, float y, float radius, int segments, int start, int degrees){
        double start_1 =  (start - 90) / 360.0 * segments;
        double end_1 = (start + degrees - 90) / 360.0 * segments;
        double ldx = radius * Math.cos(2.0 * Math.PI * start_1 / (1.0 * segments));
        double ldy = radius * Math.sin(2.0 * Math.PI * start_1 / (1.0 * segments));
        for (double i = start_1; i <= end_1; i += 1.0) {
            double angle = 2.0 * Math.PI * i / (1.0 * segments);

            double dx = radius * Math.cos(angle);
            double dy = radius * Math.sin(angle);

            shapeRenderer.line((float)(x + ldx), (float)(y + ldy), (float)(x + dx), (float)( y+ dy));
            ldx = dx;
            ldy = dy;
        }
        double angle = 2.0 * Math.PI * end_1 / (1.0 * segments);

        double dx = radius * Math.cos(angle);
        double dy = radius * Math.sin(angle);
        shapeRenderer.line((float)(x + ldx), (float)(y + ldy), (float)(x + dx), (float)( y+ dy));

    }

    public void drawHelpLines(ShapeRenderer shapeRenderer){
        if(!(pointForHelpLines.x >= planetPos.x && pointForHelpLines.x < planetPos.x + planetSize &&
                pointForHelpLines.y >= planetPos.y && pointForHelpLines.y < planetPos.y + planetSize)){
            int p, delta;
            if(pointForHelpLines.y >= planetPos.y && pointForHelpLines.y < planetPos.y + planetSize){
                p = pointForHelpLines.x;
                if (pointForHelpLines.x > planetPos.x)
                    p = planetPos.x - (pointForHelpLines.x - planetSize - planetPos.x + 1);
                delta = planetPos.x - p;

            } else {
                p = pointForHelpLines.y;
                if(pointForHelpLines.y > planetPos.y)
                    p = planetPos.y - (pointForHelpLines.y - planetSize - planetPos.y + 1);

                delta = planetPos.y - p;
            }

            drawLinedRoundRect(shapeRenderer, p * METEOR_SIZE - startPos.x, p * METEOR_SIZE - startPos.y,
                    (delta * 2 + planetSize) * METEOR_SIZE, (delta * 2 + planetSize) * METEOR_SIZE, (delta * 2 + planetSize) * METEOR_SIZE / 20f,
                    pole.getColorByIndex(SettingScreen.theme.equals(SettingScreen.ColorTheme.light) ? 0: 13));

            if(delta != 1)
                drawLinedRoundRect(shapeRenderer, (p + 1) * METEOR_SIZE - startPos.x, (p + 1) * METEOR_SIZE - startPos.y,
                        (delta * 2 + planetSize - 2) * METEOR_SIZE, (delta * 2 + planetSize - 2) * METEOR_SIZE, (delta * 2 + planetSize - 2) * METEOR_SIZE / 20f,
                        pole.getColorByIndex(SettingScreen.theme.equals(SettingScreen.ColorTheme.light) ? 0: 13));


        }
    }
    public boolean isPointInPlanet(int x, int y){
        return (x >= planet.x && x <= planet.x + planet.getWidth() && y >= planet.y && y <= planet.y + planet.getHeight());
    }
    /**
     * Инициализация всех таймеров ->
     * 1. Включен изначально таймер или нет
     * 2. Интервал работы таймера
     * 3. Что делать таймеру, каждую отрисовку экрана
     * 4. ЧТо делать таймеру после окончания работы
     */
    private void timerInit(){
        moveTimer = new MiniTimer(false, (11 - SettingScreen.speed) * 0.12f);
        moveTimer.setTasker(new Tasker() {
            @Override
            public void doAfterTime() {
                if(!pole.move(Pole.TypeOfMove.down)) {
                   doAfterFall();
                }
                if(!qLose)
                    moveTimer.enabled();
            }

            @Override
            public void onTick(float delta) {

            }
        });

        rotateTimer = new MiniTimer(false, 0.3f);
        angleVel = 90f / rotateTimer.getInterval();
        rotateTimer.setTasker(new Tasker() {
            @Override
            public void doAfterTime() {
                camera.rotate(-angle);
                camera.update();
                angle = 0f;
                rebuildPole();
                if(qLose)
                    loseFunction(); else {
                    moveTimer.enabled();
                    rotateButton.enable();
                }
            }

            @Override
            public void onTick(float delta) {
                float d = angleVel * delta;
                angle += d;
                camera.rotate(d);
                camera.update();
            }
        });

        deleteTimer = new MiniTimer(false, 0.45f);
        final float deleteCubesVel = METEOR_SIZE / 2 / deleteTimer.getInterval();
        deleteTimer.setTasker(new Tasker() {
            @Override
            public void doAfterTime() {
                deleteCubes.clear();
            }

            @Override
            public void onTick(float delta) {
                for (int i = 0; i < deleteCubes.size(); i++) {
                    deleteCubes.get(i).size -= deleteCubesVel * delta;
                }
            }
        });

        gameOverTimer = new MiniTimer(false, 1f);
        final float gameOverSpriteVel = -SCREEN_HEIGHT / 2f / gameOverTimer.getInterval();
        gameOverTimer.setTasker(new Tasker() {
            @Override
            public void doAfterTime() {
                exitButton.set_position(SCREEN_WIDTH / 2f - SCREEN_WIDTH / 10f, SCREEN_HEIGHT + 3f - SCREEN_HEIGHT / 2f);
                newGameButton1.set_position(SCREEN_WIDTH / 2f - SCREEN_WIDTH / 8f, exitButton.y + exitButton.getHeight()  + newGameButton1.getHeight() * 0.5f);
                gameOverLabel.setPosition(SCREEN_WIDTH / 2f - gameOverLabel.getWidth() / 2f, newGameButton1.y + newGameButton1.getHeight() + 1.5f * gameOverLabel.getHeight());

            }

            @Override
            public void onTick(float delta) {
                gameOverLabel.translateY(gameOverSpriteVel * delta);
                newGameButton1.translateY(gameOverSpriteVel * delta);
                exitButton.translateY(gameOverSpriteVel * delta);
            }
        });


        scoreTimer = new MiniTimer(false, 0.01f);
        scoreTimer.setTasker(new Tasker() {
            @Override
            public void doAfterTime() {
                score++;
                if(score != maxTimerScore)
                    scoreTimer.enabled();
            }

            @Override
            public void onTick(float delta) {

            }
        });

        putDownTimer = new MiniTimer(false, 0.01f);
        putDownTimer.setTasker(new Tasker() {
            @Override
            public void doAfterTime() {
                if(pole.move(Pole.TypeOfMove.down)){
                    putDownTimer.enabled();
                } else {
                    moveTimer.enabled();
                    downButton.enable();
                    doAfterFall();
                }
            }

            @Override
            public void onTick(float delta) {

            }
        });

        pauseRotateTimer = new MiniTimer(false, 0.5f);
        final float pauseAngleVel = 90f / pauseRotateTimer.getInterval();
        final float restartButtonVel = pauseButton.width / pauseRotateTimer.getInterval();
        pauseRotateTimer.setTasker(new Tasker() {
            @Override
            public void doAfterTime() {
                pauseButton.getTexture().rotate(90f - pauseAngle);
                restartButton.getTexture().rotate(180f - restartAngle);
                pauseAngle = 0f;
                restartAngle = 0f;
                if(!isPaused) {
                    restartButton.setPos(pauseButton.x - pauseButton.width * 1.2f, pauseButton.y);
                    restartButton.set_width(pauseButton.width);
                    restartButton.set_height(pauseButton.height);
                } else{
                    restartButton.setPos(pauseButton.x - pauseButton.width * 0.7f, pauseButton.y + pauseButton.height / 2f);
                    restartButton.set_width(0);
                    restartButton.set_height(0);
                }
                restartButton.visible = !isPaused;
            }

            @Override
            public void onTick(float delta) {
                float d = pauseAngleVel * delta;
                pauseAngle += d;
                restartAngle += 2 * d;
                pauseButton.getTexture().rotate(d);
                restartButton.getTexture().rotate(d * 2);
                float d_2 = restartButtonVel * delta;
                if(!isPaused)
                    d_2 = -d_2;
                restartButton.translateX(d_2 / 2f);
                restartButton.translateY(d_2 / 2f);
                restartButton.translateSize(-d_2, -d_2);
            }
        });
    }
    float pauseAngle = 0f;
    float restartAngle = 0f;
    Button newGameButton1;

    /**
     * Инициализация кнопок ->
     * 1. Координаты
     * 2. Размеры
     * 3. Текстура для отображения
     * 4. Отображения и поврот на 90
     * 5. Прозрачность текстуры
     */
    private void buttonsInit(){
        String darkS = "";
        if(SettingScreen.theme.equals(SettingScreen.ColorTheme.dark)) darkS = "_d";
        leftButton = new ImageButton(7f * SCREEN_WIDTH / 60f, SCREEN_HEIGHT / 12f, SCREEN_WIDTH / 6f, SCREEN_WIDTH / 6f,"textures/arrow"+darkS+".png", false, false, 0.5f);
        leftButton.setClikedInterface(new ClikedInterface() {
            @Override
            public void onClick() {
                pole.move(Pole.TypeOfMove.left);
            }
        });
        rightButton = new ImageButton(43f * SCREEN_WIDTH / 60f, SCREEN_HEIGHT / 12f, SCREEN_WIDTH / 6f,SCREEN_WIDTH / 6f,"textures/arrow"+darkS+".png", true, false, 0.5f);
        rightButton.setClikedInterface(new ClikedInterface() {
            @Override
            public void onClick() {
                pole.move(Pole.TypeOfMove.right);
            }
        });

        rotateButton = new ImageButton(SCREEN_WIDTH / 2f - SCREEN_WIDTH / 14f, SCREEN_HEIGHT / 12f + SCREEN_WIDTH / 8f, SCREEN_WIDTH / 7f, SCREEN_WIDTH / 7f,"textures/arrow_rotate"+darkS+".png", false, false, 0.5f);
        rotateButton.setClikedInterface(new ClikedInterface() {
            @Override
            public void onClick() {
                rotatePlanet();
            }
        });

        downButton = new ImageButton(SCREEN_WIDTH / 2f - SCREEN_WIDTH / 14f, SCREEN_HEIGHT / 12f - SCREEN_WIDTH / 8f, SCREEN_WIDTH / 7f, SCREEN_WIDTH / 7f,"textures/arrow"+darkS+".png", false, true, 0.5f);
        downButton.setClikedInterface(new ClikedInterface() {
            @Override
            public void onClick() {
                putDown();
            }
        });



        newGameButton1 = new Button(SCREEN_WIDTH / 2f - SCREEN_WIDTH / 8f, SCREEN_HEIGHT + SCREEN_WIDTH / 16f * 1.2f, SCREEN_WIDTH / 4f, SCREEN_WIDTH / 16f, "textures/button.png", MAIN_TTF, "New game", new com.badlogic.gdx.graphics.Color(1f, 1f, 1f, 1f));
        newGameButton1.setClikedInterface(new ClikedInterface() {
            @Override
            public void onClick() {
                enableButtons();
                newGame();
                pole.makeNewFigure();
            }
        });

        exitButton = new Button(SCREEN_WIDTH / 2f - SCREEN_WIDTH / 10f, SCREEN_HEIGHT + 3f, SCREEN_WIDTH / 5f, SCREEN_WIDTH / 16f, "textures/button.png", MAIN_TTF, "menu", new com.badlogic.gdx.graphics.Color(1f, 1f, 1f, 1f), newGameButton1.getTextSize());
        exitButton.setClikedInterface(new ClikedInterface() {
            @Override
            public void onClick() {
                deleteLastGameFile();
                MenuScreen.canResume = false;
                game.setScreen(game.menuScreen);
            }
        });

        //exitButton.setTextSize(newGameButton1.getTextSize());

        float k_pause_size = 0.09f;
        float delta_pause_x = SCREEN_WIDTH * k_pause_size * 0.2f;

        pauseButton = new ImageButton(SCREEN_WIDTH - delta_pause_x - SCREEN_WIDTH * k_pause_size,  SCREEN_HEIGHT - delta_pause_x - SCREEN_WIDTH * k_pause_size, SCREEN_WIDTH * k_pause_size, SCREEN_WIDTH * k_pause_size, "textures/pause"+darkS+".png", false, false, 0.5f);
        pauseButton.setClikedInterface(new ClikedInterface() {
            @Override
            public void onClick() {
                if(isPaused){
                    saveLastGame();
                    if(best_score < score)
                        best_score = score;
                    saveBestScore();
                    game.setScreen(game.menuScreen);
                    isPaused = false;
                }else
                    pauseGame(true);
            }
        });

        float x = pauseButton.x - pauseButton.width * 1.2f;
        float y = pauseButton.y;
        float w = pauseButton.width;
        float h = pauseButton.height;
//        if(typeOfOpen.equals(TypeOfOpen.resume)){
//            x += w / 2;
//            y += h / 2;
//            w = 0;
//            h = 0;
//        }
        restartButton = new ImageButton(x, y, w, h, "textures/arrow_restart"+darkS+".png", false, false, 0.5f);
        restartButton.setClikedInterface(new ClikedInterface() {
            @Override
            public void onClick() {
                loseFunction();
                enableButtons();
                gameOverTimer.disabled();
                newGame();
                pole.makeNewFigure();
            }
        });
    }

    private boolean lastEnabled[] = new boolean[4];
    public boolean isPaused = false;

    public void disableButtons(){
        restartButton.disable();
        leftButton.disable();
        rightButton.disable();
        downButton.disable();
        rotateButton.disable();
    }

    public void enableButtons(){
        restartButton.enable();
        leftButton.enable();
        rightButton.enable();
        downButton.enable();
        rotateButton.enable();
    }

    public void pauseGame(boolean pause){
        isPaused = pause;
        if(pause) {
            lastEnabled[0] = moveTimer.isEnabled();
            lastEnabled[1] = rotateTimer.isEnabled();
            lastEnabled[2] = deleteTimer.isEnabled();
            lastEnabled[3] = putDownTimer.isEnabled();

            disableButtons();

            moveTimer.disabled();
            rotateTimer.disabled();
            deleteTimer.disabled();
            putDownTimer.disabled();
            pauseLabel.setVisibility(true);
            pauseRotateTimer.enabled();
        } else{
            pauseLabel.setVisibility(false);
            if(lastEnabled[0]) moveTimer.enabled();
            if(lastEnabled[1]) rotateTimer.enabled();
            if(lastEnabled[2]) deleteTimer.enabled();
            if(lastEnabled[3]) putDownTimer.enabled();
            restartButton.visible = true;
            pauseRotateTimer.enabled();
            enableButtons();
        }
    }

    @Override
    public void show() {
        canTick = false;
        getBestScore();

        m_batch = new SpriteBatch();
        m_shapeRenderer = new ShapeRenderer();
        um_shapeRenderer = new ShapeRenderer();
        deleteCubes = new ArrayList<DeleteCube>();

        pole = new Pole(38, 30);

        METEOR_SIZE = SCREEN_HEIGHT / (float)pole.getVisibleH();
        EPSILON = METEOR_SIZE / 20f;

        startPos = new Vector2((pole.N * METEOR_SIZE - SCREEN_WIDTH) / 2,  (pole.N - pole.getVisibleH()) / 2 * METEOR_SIZE);
        camera = new OrthographicCamera(SCREEN_WIDTH, SCREEN_HEIGHT);
        camera.translate(SCREEN_WIDTH / 2, SCREEN_HEIGHT / 2);
        camera.update();

        planetSize = 4;
        planetPos = new Point(pole.N / 2 - planetSize / 2, pole.N / 2 - planetSize / 2);
        pointForHelpLines = new Point(planetPos.x + planetSize / 2, planetPos.y + planetSize / 2);

        /**
         * Клеточки на которых стоит планета мы инициилизируем как Pole.TypeOfElement.um_meteor,
         * чтобы на них падали блоки, а не пролетали мимо
         */
        for (int i = 0; i < planetSize; i++) {
            for (int j = 0; j < planetSize; j++) {
                pole.setTypeOfElement(Pole.TypeOfElement.um_meteor, pole.N / 2 - planetSize / 2 + i, pole.N / 2 - planetSize / 2 + j);
                pole.getElement(pole.N / 2 - planetSize / 2 + i, pole.N / 2 - planetSize / 2 + j).color.set(Pole.WHITE);
            }
        }

        //Иницилиазия координаты и размеры планеты в центре
        planet = new Rectangle(planetPos.x * METEOR_SIZE - startPos.x, planetPos.y * METEOR_SIZE  - startPos.y, METEOR_SIZE * planetSize, METEOR_SIZE * planetSize);


        buttonsInit();
        timerInit();

        pauseLabel = new MyLabel(MAIN_TTF, "PAUSE");
        pauseLabel.setSize(SCREEN_WIDTH / 10f);
        pauseLabel.setVisibility(false);
        pauseLabel.setPosition(SCREEN_WIDTH / 2f - pauseLabel.getWidth() / 2f, SCREEN_HEIGHT / 2f - pauseLabel.getHeight() / 2f);
        pauseLabel.setColor(SettingScreen.theme.equals(SettingScreen.ColorTheme.light) ? LIGHT_THEME_2ND_TEXT_COLOR : DARK_THEME_2ND_TEXT_COLOR);

        gameOverLabel = new MyLabel(MAIN_TTF, "Game Over");
        gameOverLabel.setSize(SCREEN_WIDTH / 8f);
        gameOverLabel.setVisibility(false);
        gameOverLabel.setPosition(SCREEN_WIDTH / 2f - pauseLabel.getWidth() / 2f, SCREEN_HEIGHT + newGameButton1.getHeight() + 0.5f * gameOverLabel.getHeight());
        gameOverLabel.setColor(SettingScreen.theme.equals(SettingScreen.ColorTheme.light) ? LIGHT_THEME_2ND_TEXT_COLOR : DARK_THEME_2ND_TEXT_COLOR);

        scoreLabel = new MyLabel(MAIN_TTF, "Score: ");
        scoreLabel.setSize(pauseButton.getHeight() / 1.5f);
        scoreLabel.setPosition((SCREEN_WIDTH - pauseButton.x) * 0.5f, pauseButton.y + scoreLabel.getHeight() * 0.5f);
        scoreLabel.setColor(SettingScreen.theme.equals(SettingScreen.ColorTheme.light) ? LIGHT_THEME_TEXT_COLOR : lightToDark(LIGHT_THEME_TEXT_COLOR));

        bestScoreLabel = new MyLabel(MAIN_TTF, "Best score: ");
        bestScoreLabel.setSize(pauseButton.getHeight() / 1.5f);
        bestScoreLabel.setPosition((SCREEN_WIDTH - pauseButton.x) * 0.5f, pauseButton.y + bestScoreLabel.getHeight() * 0.5f - scoreLabel.getHeight() * 1.2f);
        bestScoreLabel.setColor(SettingScreen.theme.equals(SettingScreen.ColorTheme.light) ? LIGHT_THEME_TEXT_COLOR : lightToDark(LIGHT_THEME_TEXT_COLOR));

        heart = new Sprite(new Texture("textures/heart.png"));
        heart.setSize(METEOR_SIZE, METEOR_SIZE * 0.895f);

        lifes = 3;
        //pole.makeNewFigure();
        //moveTimer.enabled();

        newGame();
        if(typeOfOpen.equals(TypeOfOpen.resume)) {
            pauseGame(true);
            getLastGame();
        }
        else
            pole.makeNewFigure();

        Gdx.input.setInputProcessor(new InputListener(this));
    }

    // Что мы делаем после того, как фигура упала на неподвижный блок
    private void doAfterFall(){
        if (!pole.makeNewFigure() || lifes == 0){
            qLose = true;
            loseFunction();
        } else {
            deleteCubes.clear();
            if (pole.findSquare(planetSize, deleteCubes)) {
                if (deleteCubes.size() != 0) {
                    maxTimerScore = score + deleteCubes.size();
                    scoreTimer.enabled();
                    deleteTimer.enabled();
                }
            }

            for (int i = 0; i < pole.N; i++) {
                for (int j = 0; j < (pole.N - pole.getVisibleH()) / 2; j++) {
                    pole.setTypeOfElement(Pole.TypeOfElement.empty, i, j);
                    pole.getElement(i, j).color.set(Pole.WHITE);
                }
            }
        }
    }

    //Опустить фигуру в самый низ
    public void putDown(){
        moveTimer.disabled();
        putDownTimer.enabled();
        downButton.disable();
    }

    //Пореворачиваем массив
    private void rebuildPole(){

        ArrayList<ColorPoint> moved_blocks = new ArrayList<ColorPoint>();

        for (int i = 0; i < pole.N; i++) {
            for (int j = 0; j < pole.N; j++) {
                if(pole.getElement(i, j).type.equals(Pole.TypeOfElement.m_meteor)){
                    moved_blocks.add(new ColorPoint(i, j, pole.getElement(i, j).color));
                    pole.setTypeOfElement(Pole.TypeOfElement.empty, i, j);
                }
            }
        }

        Pole.Element[][] a = new Pole.Element[pole.N][pole.N];
        for (int i = 0; i < pole.N; i++) {
            for (int j = 0; j < pole.N; j++) {
                a[i][j] = new Pole.Element(pole.getElement(i, j));
            }
        }

        int p = pole.N / 2;
        int n = pole.N;
        for(int r = 1; r <= p; r++) {
            for(int c = r; c <= n - r; c++) {
                Pole.Element x = new Pole.Element(a[r - 1][c - 1].type, a[r - 1][c - 1].color.R, a[r - 1][c - 1].color.G, a[r - 1][c - 1].color.B);
                a[r - 1][c - 1].set(a[c - 1][n - r]);
                a[c - 1][n - r].set(a[n - r][n - c]);
                a[n - r][n - c].set(a[n - c][r - 1]);
                a[n - c][r - 1].set(x);
            }
        }

        for (int i = 0; i < pole.N; i++) {
            for (int j = 0; j < pole.N; j++) {
                pole.setElement(a[i][j], i, j);
            }
        }

        for (int i = 0; i < moved_blocks.size(); i++) {
            if(pole.getElement(moved_blocks.get(i).x, moved_blocks.get(i).y).type.equals(Pole.TypeOfElement.um_meteor)){
                qLose = true;
            }
            pole.setTypeOfElement(Pole.TypeOfElement.m_meteor, moved_blocks.get(i).x, moved_blocks.get(i).y);
            pole.getElement(moved_blocks.get(i).x, moved_blocks.get(i).y).color.set(moved_blocks.get(i).color);
        }
    }

    //Поворот планеты
    public void rotatePlanet(){
        moveTimer.disabled();
        rotateButton.disable();
        rotateTimer.enabled();
    }


    private void getBestScore(){
        FileHandle file = Gdx.files.external(ROOT_DIRECTORY+"/bestresult.txt");
        if(file.exists()){
            String s = file.readString();
            best_score = Integer.parseInt(s);
        }else{
            best_score = 0;
        }
    }

    private void saveBestScore(){
        FileHandle file = Gdx.files.external(ROOT_DIRECTORY+"bestresult.txt");
        if(file.exists())
            file.delete();
        file.writeString(String.valueOf(best_score), false);
    }

    public void getLastGame(){
        FileHandle file = Gdx.files.external(ROOT_DIRECTORY+"lastgame.txt");
        if(file.exists()){
            String s = file.readString();
            String str[] = s.split("\n");
            for (int i = 0; i < str.length - 1; i++) {
                String stroke[] = str[i].split(" ");
                int x = Integer.parseInt(stroke[0]);
                int y = Integer.parseInt(stroke[1]);
                int ind = Integer.parseInt(stroke[2]);
                Pole.TypeOfElement t = Pole.TypeOfElement.um_meteor;
                if(stroke[3].equals("m"))
                    t = Pole.TypeOfElement.m_meteor;
                pole.setTypeOfElement(t, x, y);
                pole.getElement(x, y).color.set(pole.getColorByIndex(ind));
                if(t.equals(Pole.TypeOfElement.m_meteor))
                    pole.addMoveBlock(x, y, pole.getColorByIndex(ind));
            }
            String stroke[] = str[str.length - 1].split(" ");
            score = Integer.parseInt(stroke[0]);
            lifes = Integer.parseInt(stroke[1]);
        }
    }
    public void saveLastGame(){
        FileHandle file = Gdx.files.external(ROOT_DIRECTORY+"lastgame.txt");
        if(file.exists())
            file.delete();
        String f = "";
        for (int i = 0; i < pole.N; i++) {
            for (int j = 0; j < pole.N; j++) {
                if(!pole.getElement(i, j).type.equals(Pole.TypeOfElement.empty)){
                    String t;
                    if(pole.getElement(i, j).type.equals(Pole.TypeOfElement.um_meteor))
                        t = "u"; else
                        t = "m";
                    f += String.valueOf(i) +" "+String.valueOf(j)+" "+String.valueOf(pole.getIndexByColor(pole.getElement(i, j).color)) + " " + t + "\n";
                }
            }
        }
        f += String.valueOf(score) + " " + String.valueOf(lifes);
        file.writeString(f, true);
    }
    /**
     *
     * @param shapeRenderer
     * ShapeRenderer на котором будут отображатся фигуры типа t
     * @param t
     * Тип фигур, которые мы будем рисовать
     */
    private void drawElements(ShapeRenderer shapeRenderer, Pole.TypeOfElement t){

        for (int i = 0; i < pole.N; i++) {
            for (int j = 0; j < pole.N; j++) {
                if(!pole.getElement(i, j).type.equals(Pole.TypeOfElement.empty)){
                    if(pole.getElement(i, j).type.equals(t)){
                        if(!(t.equals(Pole.TypeOfElement.um_meteor) && i >= planetPos.x && i < planetPos.x + planetSize && j >= planetPos.y && j < planetPos.y + planetSize)) {

                            float x = -startPos.x + i * METEOR_SIZE + EPSILON;
                            float y = -startPos.y + j * METEOR_SIZE + EPSILON;
                            float size = METEOR_SIZE - 2 * EPSILON;
                            drawRoundRect(shapeRenderer, x, y, size, size, METEOR_SIZE / 10f, pole.getElement(i, j).color);
                        }
                    }
                }
            }
        }

    }

    private void drawScore(){
        m_batch.begin();

        scoreLabel.setText("Score: " + String.valueOf(score));
        scoreLabel.draw(m_batch);

        bestScoreLabel.setText("Best score: " + String.valueOf(best_score));
        bestScoreLabel.draw(m_batch);

        m_batch.end();
    }

    //Тик всех таймеров
    private void tickTimers(float delta){
        moveTimer.tick(delta);
        rotateTimer.tick(delta);
        deleteTimer.tick(delta);
        gameOverTimer.tick(delta);
        putDownTimer.tick(delta);
        scoreTimer.tick(delta);
        pauseRotateTimer.tick(delta);
    }

    private void drawButtons(SpriteBatch batch){
        if(!qLose) {
            leftButton.draw(batch);
            rightButton.draw(batch);
            rotateButton.draw(batch);
            downButton.draw(batch);
            restartButton.draw(batch);
        }
        pauseButton.draw(batch);
    }

    boolean canTick;
    @Override
    public void render(float delta) {
        if(SettingScreen.theme.equals(SettingScreen.ColorTheme.light))
            Gdx.gl.glClearColor(LIGHT_THEME_BACKGROUND_COLOR.r, LIGHT_THEME_BACKGROUND_COLOR.g, LIGHT_THEME_BACKGROUND_COLOR.b, 1f); else
            Gdx.gl.glClearColor(DARK_THEME_BACKGROUND_COLOR.r, DARK_THEME_BACKGROUND_COLOR.g, DARK_THEME_BACKGROUND_COLOR.b, 1f);

        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        /**
         * Ставим камеру на um_shapeRenderer, потому что при повороте камеры должны
         * поворачиваться лишь фигуры, которые уже стоят на планете
         */
        um_shapeRenderer.setProjectionMatrix(camera.combined);

        //Рисуем неподвижные блоки
        drawElements(um_shapeRenderer, Pole.TypeOfElement.um_meteor);

        //Рисуем движущиеся блоки
        drawElements(m_shapeRenderer, Pole.TypeOfElement.m_meteor);

        //Рисуем планету
        drawRoundRect(um_shapeRenderer, planet.getX(), planet.getY(), planet.getWidth(), planet.getHeight(), planet.getWidth() * 0.1f, pole.getColorByIndex(SettingScreen.theme.equals(SettingScreen.ColorTheme.light) ? 0: 13));

        float EPSILON = METEOR_SIZE / 20f;

        for (int i = 0; i < deleteCubes.size(); i++) {
            drawRoundRect(um_shapeRenderer, -startPos.x + deleteCubes.get(i).x * METEOR_SIZE + EPSILON + (METEOR_SIZE - deleteCubes.get(i).size) / 2, -startPos.y + deleteCubes.get(i).y * METEOR_SIZE + EPSILON+ (METEOR_SIZE - deleteCubes.get(i).size) / 2, deleteCubes.get(i).size - 2 * EPSILON, deleteCubes.get(i).size - 2 * EPSILON, deleteCubes.get(i).size / 10, deleteCubes.get(i).color);
        }

        Gdx.gl.glEnable(GL20.GL_BLEND);
        Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);

        drawButtons(m_batch);

        if(qLose){
            m_batch.begin();
            gameOverLabel.draw(m_batch);
            m_batch.end();
        }

        //newGameButton.draw(m_batch);
        m_batch.begin();
        newGameButton1.draw(m_batch);
        exitButton.draw(m_batch);
        m_batch.end();

        if(isPaused){
            m_batch.begin();
            pauseLabel.draw(m_batch);
            m_batch.end();
        }

        drawScore();

        if(!qLose) {
            m_batch.begin();
            for (int i = 0; i < lifes; i++) {
                heart.setPosition(bestScoreLabel.getX() + (0.01596f * SCREEN_WIDTH + heart.getWidth()) * i, bestScoreLabel.getY() - heart.getHeight() * 1.2f);
                heart.draw(m_batch);
            }
            m_batch.end();
        }
        Gdx.gl.glDisable(GL20.GL_BLEND);

        if(needDrawHelpLines){
            drawHelpLines(um_shapeRenderer);
        }
        if(canTick) {
            if(!needDrawHelpLines)
                tickTimers(delta);
        }else
            canTick = true;
    }

    @Override
    public void resize(int width, int height) {
        if(MenuScreen.canResume)
            saveLastGame();
    }

    @Override
    public void pause() {
        if(MenuScreen.canResume)
            saveLastGame();
    }

    @Override
    public void resume() {
        if(MenuScreen.canResume)
            saveLastGame();
    }

    @Override
    public void hide() {
        if(MenuScreen.canResume)
            saveLastGame();
    }

    @Override
    public void dispose() {
//        if(MenuScreen.canResume)
//            saveLastGame();
        m_batch.dispose();
        m_shapeRenderer.dispose();
        um_shapeRenderer.dispose();
    }
}
