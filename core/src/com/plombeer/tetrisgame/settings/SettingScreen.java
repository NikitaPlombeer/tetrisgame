package com.plombeer.tetrisgame.settings;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.plombeer.tetrisgame.ClikedInterface;
import com.plombeer.tetrisgame.GameScreen;
import com.plombeer.tetrisgame.ImageButton;
import com.plombeer.tetrisgame.MyGdxGame;
import com.plombeer.tetrisgame.util.MyLabel;

/**
 * Created by hh on 11.07.2015.
 */
public class SettingScreen implements Screen {

    public enum ColorTheme{
        dark, light
    }

    public static ColorTheme theme = ColorTheme.light;
    public final float FIRST_BORDER_X = GameScreen.SCREEN_WIDTH * 0.03333f;
    public final float SECOND_BORDER_X = GameScreen.SCREEN_WIDTH * 0.15555f;
    public final float D_BETWEEN_INC_BUTTONS = 0.398148f * GameScreen.SCREEN_WIDTH;
    public final float DELTA_X = SECOND_BORDER_X - FIRST_BORDER_X;
    public float DELTA_Y = SECOND_BORDER_X - FIRST_BORDER_X + GameScreen.SCREEN_HEIGHT;

    public ImageButton incButton;
    public ImageButton decButton;
    public ImageButton darkThemeButton;
    public ImageButton lightThemeButton;
    public ImageButton menuButton;


    public SpriteBatch batch;
    MyLabel fallingSpeedTextLabel;
    MyLabel fallingSpeedLabel;
    MyLabel darkThemeLabel;
    MyLabel themeTextLabel;
    MyLabel lightThemeLabel;

    MyGdxGame game;

    public SettingScreen(MyGdxGame game) {
        this.game = game;
    }

    public static int speed = 5;


    public static void getSettings(){
        FileHandle file = Gdx.files.external("NewTetris/settings.set");
        if(file.exists()){
            String s = file.readString();
            String str[] = s.split(" ");
            speed = Integer.parseInt(str[0]);
            theme = str[1].equals("d") ? ColorTheme.dark : ColorTheme.light;
        }

    }

    private void saveSetting(){
        FileHandle file = Gdx.files.external("NewTetris/settings.set");
        if(file.exists())
            file.delete();
        file.writeString(String.valueOf(speed) +" "+ (theme.equals(ColorTheme.dark) ? "d" : "l"), false);
    }


    public final String checkBoxName = "checkbox_2";
    public void changeColors(){
        Color ntText = new Color(SettingScreen.theme.equals(SettingScreen.ColorTheme.light) ? GameScreen.LIGHT_THEME_TEXT_COLOR : GameScreen.DARK_THEME_TEXT_COLOR);
        ntText.a = 1f;
        fallingSpeedTextLabel.setColor(ntText);
        fallingSpeedLabel.setColor(SettingScreen.theme.equals(SettingScreen.ColorTheme.light) ? GameScreen.LIGHT_THEME_2ND_TEXT_COLOR : GameScreen.DARK_THEME_2ND_TEXT_COLOR);
        darkThemeLabel.setColor(ntText);
        lightThemeLabel.setColor(ntText);
        themeTextLabel.setColor(ntText);
        String darkS = "";
        if(theme.equals(SettingScreen.ColorTheme.dark)) darkS = "_d";
        incButton.changeTexture("textures/settings/incer_button"+darkS+".png");
        decButton.changeTexture("textures/settings/incer_button"+darkS+".png");
        menuButton.changeTexture("textures/settings/menu"+darkS+".png");
    }


    @Override
    public void show() {
        Color ntText = new Color(SettingScreen.theme.equals(SettingScreen.ColorTheme.light) ? GameScreen.LIGHT_THEME_TEXT_COLOR : GameScreen.DARK_THEME_TEXT_COLOR);
        ntText.a = 1f;

        String darkS = "";
        if(theme.equals(SettingScreen.ColorTheme.dark)) darkS = "_d";

        batch = new SpriteBatch();
        fallingSpeedTextLabel = new MyLabel(GameScreen.MAIN_TTF, "falling speed:");
        fallingSpeedTextLabel.setWidth(0.55555f * GameScreen.SCREEN_WIDTH);
        DELTA_Y = fallingSpeedTextLabel.getSize();
        fallingSpeedTextLabel.setColor(ntText);
        fallingSpeedTextLabel.setPosition(FIRST_BORDER_X + DELTA_X, GameScreen.SCREEN_HEIGHT / 2f - fallingSpeedTextLabel.getHeight() / 2f - DELTA_Y);


        decButton = new ImageButton(SECOND_BORDER_X + DELTA_X, 0.33697f * GameScreen.SCREEN_HEIGHT - DELTA_Y, GameScreen.SCREEN_WIDTH * 0.09375f, GameScreen.SCREEN_WIDTH * 0.16875f, "textures/settings/incer_button"+darkS+".png", false, false, 1f);
        incButton = new ImageButton(SECOND_BORDER_X + DELTA_X + decButton.getWidth() + D_BETWEEN_INC_BUTTONS, 0.33697f * GameScreen.SCREEN_HEIGHT - DELTA_Y,GameScreen.SCREEN_WIDTH * 0.09375f, GameScreen.SCREEN_WIDTH * 0.16875f, "textures/settings/incer_button"+darkS+".png", true, false, 1f);

        decButton.setClikedInterface(new ClikedInterface() {
            @Override
            public void onClick() {
                speed--;
                if(speed == 0) speed = 1;
                fallingSpeedLabel.setText(String.valueOf(speed));
                fallingSpeedLabel.setPosition(decButton.x + decButton.width + D_BETWEEN_INC_BUTTONS / 2f - fallingSpeedLabel.getWidth() / 2f,
                        decButton.y + decButton.height / 2f - fallingSpeedLabel.getHeight() / 2f);
            }
        });

        incButton.setClikedInterface(new ClikedInterface() {
            @Override
            public void onClick() {
                speed++;
                if(speed == 11) speed = 10;
                fallingSpeedLabel.setText(String.valueOf(speed));
                fallingSpeedLabel.setPosition(decButton.x + decButton.width + D_BETWEEN_INC_BUTTONS / 2f - fallingSpeedLabel.getWidth() / 2f,
                        decButton.y + decButton.height / 2f - fallingSpeedLabel.getHeight() / 2f);
            }
        });

        fallingSpeedLabel = new MyLabel(GameScreen.MAIN_TTF, String.valueOf(speed));
        fallingSpeedLabel.setSize(fallingSpeedTextLabel.getSize());
        fallingSpeedLabel.setColor(SettingScreen.theme.equals(SettingScreen.ColorTheme.light) ? GameScreen.LIGHT_THEME_2ND_TEXT_COLOR : GameScreen.DARK_THEME_2ND_TEXT_COLOR);
        fallingSpeedLabel.setPosition(decButton.x + decButton.width + D_BETWEEN_INC_BUTTONS / 2f - fallingSpeedLabel.getWidth() / 2f,
                decButton.y + decButton.height / 2f - fallingSpeedLabel.getHeight() / 2f);



        lightThemeButton = new ImageButton(SECOND_BORDER_X + DELTA_X, 0.7796875f * GameScreen.SCREEN_HEIGHT - DELTA_Y, 0.15f * GameScreen.SCREEN_WIDTH, 0.15f * GameScreen.SCREEN_WIDTH, "textures/settings/"+checkBoxName+"_"+ (theme.equals(ColorTheme.light) ? "t" : "f")+darkS+".png", false, false, 1f);

        darkThemeButton = new ImageButton(SECOND_BORDER_X + DELTA_X, 0.6651f * GameScreen.SCREEN_HEIGHT - DELTA_Y, 0.15f * GameScreen.SCREEN_WIDTH, 0.15f * GameScreen.SCREEN_WIDTH, "textures/settings/"+checkBoxName+"_"+ (theme.equals(ColorTheme.light) ? "f" : "t")+darkS+".png", false, false, 1f);

        lightThemeButton.setClikedInterface(new ClikedInterface() {
            @Override
            public void onClick() {
                if(theme.equals(ColorTheme.dark)){
                    lightThemeButton.changeTexture("textures/settings/"+checkBoxName+"_t.png");
                    darkThemeButton.changeTexture("textures/settings/"+checkBoxName+"_f.png");
                    theme = ColorTheme.light;
                    changeColors();
                }
            }
        });


        darkThemeButton.setClikedInterface(new ClikedInterface() {
            @Override
            public void onClick() {
                if(theme.equals(ColorTheme.light)) {
                    lightThemeButton.changeTexture("textures/settings/"+checkBoxName+"_f_d.png");
                    darkThemeButton.changeTexture("textures/settings/"+checkBoxName+"_t_d.png");
                    theme = ColorTheme.dark;
                    changeColors();
                }
            }
        });

        darkThemeLabel = new MyLabel(GameScreen.MAIN_TTF, "dark");
        darkThemeLabel.setSize(fallingSpeedTextLabel.getSize());
        darkThemeLabel.setColor(ntText);
        darkThemeLabel.setPosition(fallingSpeedLabel.getX(), darkThemeButton.y + darkThemeButton.height / 2f - darkThemeLabel.getHeight() / 2f);
        
        lightThemeLabel = new MyLabel(GameScreen.MAIN_TTF, "light");
        lightThemeLabel.setSize(fallingSpeedTextLabel.getSize());
        lightThemeLabel.setColor(ntText);
        lightThemeLabel.setPosition(fallingSpeedLabel.getX(), lightThemeButton.y + lightThemeButton.height / 2f - lightThemeLabel.getHeight() / 2f);


        themeTextLabel = new MyLabel(GameScreen.MAIN_TTF, "theme:");
        themeTextLabel.setColor(ntText);
        themeTextLabel.setSize(fallingSpeedTextLabel.getSize());
        themeTextLabel.setPosition(FIRST_BORDER_X + DELTA_X, 0.922916f * GameScreen.SCREEN_HEIGHT - DELTA_Y);

        menuButton = new ImageButton(FIRST_BORDER_X / 2f, GameScreen.SCREEN_HEIGHT - 0.094f * GameScreen.SCREEN_WIDTH - FIRST_BORDER_X / 2f, 0.094f * GameScreen.SCREEN_WIDTH, 0.094f * GameScreen.SCREEN_WIDTH, "textures/settings/menu"+darkS+".png", false, false, 1f);
        menuButton.setClikedInterface(new ClikedInterface() {
            @Override
            public void onClick() {
                saveSetting();
                game.setScreen(game.menuScreen);
            }
        });


        Gdx.input.setInputProcessor(new InputListener(this));
    }
    @Override
    public void render(float delta) {
        if(theme.equals(SettingScreen.ColorTheme.light))
            Gdx.gl.glClearColor(GameScreen.LIGHT_THEME_BACKGROUND_COLOR.r, GameScreen.LIGHT_THEME_BACKGROUND_COLOR.g, GameScreen.LIGHT_THEME_BACKGROUND_COLOR.b, 1f); else
            Gdx.gl.glClearColor(GameScreen.DARK_THEME_BACKGROUND_COLOR.r, GameScreen.DARK_THEME_BACKGROUND_COLOR.g, GameScreen.DARK_THEME_BACKGROUND_COLOR.b, 1f);

        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        batch.begin();
        fallingSpeedTextLabel.draw(batch);
        fallingSpeedLabel.draw(batch);
        darkThemeLabel.draw(batch);
        lightThemeLabel.draw(batch);
        themeTextLabel.draw(batch);
        batch.end();

        decButton.draw(batch);
        incButton.draw(batch);
        lightThemeButton.draw(batch);
        darkThemeButton.draw(batch);
        menuButton.draw(batch);

//        helloTimer.tick(delta);
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }
}
