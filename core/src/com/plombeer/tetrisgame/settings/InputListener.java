package com.plombeer.tetrisgame.settings;

import com.badlogic.gdx.InputProcessor;
import com.plombeer.tetrisgame.GameScreen;
import com.plombeer.tetrisgame.ImageButton;

/**
 * Created by hh on 11.07.2015.
 */
public class InputListener implements InputProcessor{
    ImageButton iButton;
    SettingScreen settingScreen;

    public InputListener(SettingScreen settingScreen) {
        this.settingScreen = settingScreen;
    }

    @Override
    public boolean keyDown(int keycode) {
        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        if(settingScreen.incButton.onTouch(screenX, GameScreen.SCREEN_HEIGHT - screenY)) iButton = settingScreen.incButton;
        if(settingScreen.decButton.onTouch(screenX, GameScreen.SCREEN_HEIGHT - screenY)) iButton = settingScreen.decButton;
        if(settingScreen.lightThemeButton.onTouch(screenX, GameScreen.SCREEN_HEIGHT - screenY)) iButton = settingScreen.lightThemeButton;
        if(settingScreen.darkThemeButton.onTouch(screenX, GameScreen.SCREEN_HEIGHT - screenY)) iButton = settingScreen.darkThemeButton;
        if(settingScreen.menuButton.onTouch(screenX, GameScreen.SCREEN_HEIGHT - screenY)) iButton = settingScreen.menuButton;
        return true;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        if(iButton != null){
            if(iButton.equals(settingScreen.incButton)) settingScreen.incButton.onClick(screenX, GameScreen.SCREEN_HEIGHT - screenY);
            if(iButton.equals(settingScreen.decButton)) settingScreen.decButton.onClick(screenX, GameScreen.SCREEN_HEIGHT - screenY);
            if(iButton.equals(settingScreen.lightThemeButton)) settingScreen.lightThemeButton.onClick(screenX, GameScreen.SCREEN_HEIGHT - screenY);
            if(iButton.equals(settingScreen.darkThemeButton)) settingScreen.darkThemeButton.onClick(screenX, GameScreen.SCREEN_HEIGHT - screenY);
            if(iButton.equals(settingScreen.menuButton)) settingScreen.menuButton.onClick(screenX, GameScreen.SCREEN_HEIGHT - screenY);

        }
        iButton = null;
        return true;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }
}
