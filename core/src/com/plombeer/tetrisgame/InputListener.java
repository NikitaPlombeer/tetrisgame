package com.plombeer.tetrisgame;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;

/**
 * Created by hh on 12.06.2015.
 */
public class InputListener implements InputProcessor{

    GameScreen gameScreen;
    ImageButton iButton;
    public InputListener(GameScreen gameScreen) {
        this.gameScreen = gameScreen;
    }

    @Override
    public boolean keyDown(int keycode) {
        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        if(keycode == Input.Keys.LEFT){
            gameScreen.getPole().move(Pole.TypeOfMove.left);
        }else
        if(keycode == Input.Keys.RIGHT){
            gameScreen.getPole().move(Pole.TypeOfMove.right);
        }
        else
        if(keycode == Input.Keys.DOWN){
            gameScreen.putDown();//move(Pole.TypeOfMove.down);
        }
        else
        if(keycode == Input.Keys.UP){
            gameScreen.rotatePlanet();
        }
        return true;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        if(!gameScreen.qLose) {
            if(gameScreen.pauseButton.onTouch(screenX, GameScreen.SCREEN_HEIGHT - screenY)) iButton = gameScreen.pauseButton;
            if(!gameScreen.isPaused) {
                if (gameScreen.rightButton.onTouch(screenX, GameScreen.SCREEN_HEIGHT - screenY))
                    iButton = gameScreen.rightButton;
                if (gameScreen.leftButton.onTouch(screenX, GameScreen.SCREEN_HEIGHT - screenY))
                    iButton = gameScreen.leftButton;
                if (gameScreen.downButton.onTouch(screenX, GameScreen.SCREEN_HEIGHT - screenY))
                    iButton = gameScreen.downButton;
                if (gameScreen.rotateButton.onTouch(screenX, GameScreen.SCREEN_HEIGHT - screenY))
                    iButton = gameScreen.rotateButton;
                if (gameScreen.restartButton.onTouch(screenX, GameScreen.SCREEN_HEIGHT - screenY))
                    iButton = gameScreen.restartButton;
                if (gameScreen.downButton.onTouch(screenX, GameScreen.SCREEN_HEIGHT - screenY))
                    iButton = gameScreen.downButton;
            }
        }
        if(!gameScreen.isPaused && gameScreen.isPointInPlanet(screenX, GameScreen.SCREEN_HEIGHT - screenY)){
            gameScreen.needDrawHelpLines = true;
        }
        return true;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        if(gameScreen.isPaused) {
            if(!(iButton != null && iButton.equals(gameScreen.pauseButton) && gameScreen.pauseButton.onClick(screenX, GameScreen.SCREEN_HEIGHT - screenY)))
                gameScreen.pauseGame(false);
        }
        else {
            if (!gameScreen.qLose && iButton != null) {

                if (iButton.equals(gameScreen.rightButton))
                    gameScreen.rightButton.onClick(screenX, GameScreen.SCREEN_HEIGHT - screenY);
                if (iButton.equals(gameScreen.leftButton))
                    gameScreen.leftButton.onClick(screenX, GameScreen.SCREEN_HEIGHT - screenY);
                if (iButton.equals(gameScreen.downButton))
                    gameScreen.downButton.onClick(screenX, GameScreen.SCREEN_HEIGHT - screenY);
                if (iButton.equals(gameScreen.rotateButton))
                    gameScreen.rotateButton.onClick(screenX, GameScreen.SCREEN_HEIGHT - screenY);
                if (iButton.equals(gameScreen.restartButton))
                    gameScreen.restartButton.onClick(screenX, GameScreen.SCREEN_HEIGHT - screenY);
                if (iButton.equals(gameScreen.pauseButton))
                    gameScreen.pauseButton.onClick(screenX, GameScreen.SCREEN_HEIGHT - screenY);
                if (iButton.equals(gameScreen.downButton))
                    gameScreen.downButton.onClick(screenX, GameScreen.SCREEN_HEIGHT - screenY);
            }
            gameScreen.newGameButton1.onClick(screenX, GameScreen.SCREEN_HEIGHT - screenY);
            gameScreen.exitButton.onClick(screenX, GameScreen.SCREEN_HEIGHT - screenY);
        }
//        if(!gameScreen.qGameNotStart && !gameScreen.qLose && iButton != null && iButton.equals(gameScreen.pauseButton))
//            gameScreen.pauseButton.onClick(screenX, GameScreen.SCREEN_HEIGHT - screenY);
        iButton = null;
        gameScreen.needDrawHelpLines = false;
        gameScreen.pointForHelpLines.set(gameScreen.getPlanetPos().x + gameScreen.getPlanetSize() / 2, gameScreen.getPlanetPos().y + gameScreen.getPlanetSize() / 2);
        return true;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        if(gameScreen.needDrawHelpLines){
            gameScreen.countUpPointForHelpLines(screenX, GameScreen.SCREEN_HEIGHT - screenY);
        }
        return true;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }
}
