package com.plombeer.tetrisgame.android;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.view.ViewGroup;

import com.badlogic.gdx.backends.android.AndroidFragmentApplication;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.purplebrain.adbuddiz.sdk.AdBuddiz;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class AndroidLauncher extends FragmentActivity implements AndroidFragmentApplication.Callbacks {

    //private AdView adView;
    private GameFragment gameFragment;
    //private static final String TAG = "AndroidLauncher";

    public boolean isOnline() {
        ConnectivityManager cm =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }

    @Override
	protected void onCreate (Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		//initialize(new MyGdxGame(), config);
//        requestWindowFeature(Window.FEATURE_NO_TITLE);
//        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
//                WindowManager.LayoutParams.FLAG_FULLSCREEN);
//
//        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
//            getWindow().getDecorView().setSystemUiVisibility(View.STATUS_BAR_VISIBLE);
//            getWindow().getDecorView().setSystemUiVisibility(View.STATUS_BAR_HIDDEN);
//        }
//                if(isOnline()) {
//                    setContentView(R.layout.main_layout);
//
//                    AdBuddiz.setPublisherKey("adfde944-b2d1-4cd7-9c6f-0624892aaae6");
//                    AdBuddiz.cacheAds(this); // this = current Activity
//
//                    gameFragment = new GameFragment(this);
//
//                    FragmentTransaction tr = getSupportFragmentManager().beginTransaction();
//                    tr.replace(R.id.GameView, gameFragment);
//                    tr.commit();
//
//                    setAdMob();
//                } else{

            setContentView(R.layout.main_layout_na);

            if(isOnline()){
                AdBuddiz.setPublisherKey("adfde944-b2d1-4cd7-9c6f-0624892aaae6");
                AdBuddiz.cacheAds(this); // this = current Activity
            }
            gameFragment = new GameFragment(this);
            FragmentTransaction tr = getSupportFragmentManager().beginTransaction();
            tr.replace(R.id.GameView, gameFragment);
            tr.commit();
                //   }
	}

//    private void setAdMob(){
//
//        adView = (AdView) findViewById(R.id.adView);
//
//        ViewGroup.LayoutParams lp = adView.getLayoutParams();
//        lp.height = ViewGroup.LayoutParams.WRAP_CONTENT;
//        adView.setLayoutParams(lp);
//
//        String android_id = Settings.Secure.getString(this.getContentResolver(), "android_id");
//        Object obj;
//        try
//        {
//            ((MessageDigest)(obj = MessageDigest.getInstance("MD5"))).update(
//                    android_id.getBytes(), 0, android_id.length());
//            obj = String.format("%032X", new BigInteger(1, ((MessageDigest)obj).digest()));
//        }
//        catch(NoSuchAlgorithmException localNoSuchAlgorithmException){
//            obj = android_id.substring(0, 32);
//        }
//
//        AdRequest adRequest = new AdRequest.Builder()
//                .tagForChildDirectedTreatment(true)
//                .build();
//
//        adView.loadAd(adRequest);
//        adView.setAdListener(new AdListener() {
//            @Override
//            public void onAdLoaded() {
//                super.onAdLoaded();
//                adView.setAdListener(null);
//            }
//        });
//    }
    @Override
    public void exit() {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        gameFragment.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onPause() {
  //      if (adView != null) adView.pause();
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
//        if (adView != null) adView.resume();
    }

    @Override
    public void onDestroy() {
        //if (adView != null) adView.destroy();
        super.onDestroy();
    }
}
