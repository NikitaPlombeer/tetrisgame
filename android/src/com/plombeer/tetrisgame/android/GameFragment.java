package com.plombeer.tetrisgame.android;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;
import com.badlogic.gdx.backends.android.AndroidFragmentApplication;
import com.plombeer.tetrisgame.MyGdxGame;
import com.plombeer.tetrisgame.util.ShowAds;
import com.purplebrain.adbuddiz.sdk.AdBuddiz;

/**
 * Created by hh on 09.07.2015.
 */
public class GameFragment extends AndroidFragmentApplication {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    AndroidLauncher androidLauncher;

    public GameFragment(){

    }
    @SuppressLint("ValidFragment")
    public GameFragment(AndroidLauncher androidLauncher) {
        this.androidLauncher = androidLauncher;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        AndroidApplicationConfiguration config = new AndroidApplicationConfiguration();
        //config.useImmersiveMode = true;
        return initializeForView(new MyGdxGame(new ShowAds() {
            @Override
            public void show() {
                AdBuddiz.showAd(androidLauncher); // this = current Activity
            }
        }), config);
    }

    public void onActivityResult(int request, int response, Intent data) {
        super.onActivityResult(request, response, data);
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}
